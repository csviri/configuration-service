package com.csviri.configservice.api.model.paramter;

import java.util.Date;
import java.util.Map;

public class MapParameter extends Parameter {

	public MapParameter() {
	}

	public MapParameter(String name, String label, Date validFrom, Date validTo, int version, Map<String, String> value) {
		super(name, label, validFrom, validTo, version);
		this.value = value;
	}

	private Map<String,String> value;

	public Map<String, String> getValue() {
		return value;
	}

	public void setValue(Map<String, String> value) {
		this.value = value;
	}
}
