package com.csviri.configservice.api.model.paramter;

import java.util.Date;
import java.util.List;

public class StringListParameter extends Parameter{

	public StringListParameter() {
	}

	public StringListParameter(String name, String label, Date validFrom, Date validTo, int version, List<String> value) {
		super(name, label, validFrom, validTo, version);
		this.value = value;
	}

	private List<String> value;

	public List<String> getValue() {
		return value;
	}

	public void setValue(List<String> value) {
		this.value = value;
	}
}
