package com.csviri.configservice.api.model.paramter;

import java.util.Date;

public class StringParameter extends Parameter {

	private String value;

	public StringParameter() {
	}

	public StringParameter(String name, String label, Date validFrom, Date validTo, int version, String value) {
		super(name, label, validFrom, validTo, version);
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
