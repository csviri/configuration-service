package com.csviri.configservice.api.model.paramter;

public enum ParameterType {
	STRING,
	STRING_LIST,
	MAP
}
