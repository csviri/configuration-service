package com.csviri.configservice.api.model.label;

public class DefaultLabel {

	private String defaultLabel;

	public String getDefaultLabel() {
		return defaultLabel;
	}

	public void setDefaultLabel(String defaultLabel) {
		this.defaultLabel = defaultLabel;
	}
}
