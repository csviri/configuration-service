package com.csviri.configservice.api.model.paramter;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type", defaultImpl = ParameterType.class)
@JsonSubTypes({
		@JsonSubTypes.Type(value = StringParameter.class, name = "STRING"),
		@JsonSubTypes.Type(value = StringListParameter.class, name = "STRING_LIST"),
		@JsonSubTypes.Type(value = MapParameter.class, name = "MAP")
})
public abstract class Parameter {

	private String name;

	private String label;

	private Date validFrom;

	private Date validTo;

	private int version;

	public Parameter(String name, String label, Date validFrom, Date validTo, int version) {
		this.name = name;
		this.label = label;
		this.validFrom = validFrom;
		this.validTo = validTo;
		this.version = version;
	}

	public Parameter() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
}
