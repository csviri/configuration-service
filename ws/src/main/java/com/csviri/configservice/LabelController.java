package com.csviri.configservice;


import javax.annotation.Resource;

import com.csviri.configservice.api.model.label.DefaultLabel;
import com.csviri.configservice.parameters.LabelService;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LabelController {

	@Resource
	private LabelService labelService;

	@RequestMapping(value = "/label", method = RequestMethod.PUT)
	@ResponseBody
	@ResponseStatus(code = HttpStatus.OK)
	public void setDefaultLabel(@RequestBody DefaultLabel defaultLabel) {
		if (defaultLabel.getDefaultLabel() == null || "".equals(defaultLabel.getDefaultLabel().trim())) {
			// todo own exception with status code
			throw new IllegalArgumentException("not valid label format");
		}
		labelService.setDefaultLabel(defaultLabel.getDefaultLabel());
	}

}
