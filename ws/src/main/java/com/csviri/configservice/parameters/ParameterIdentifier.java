package com.csviri.configservice.parameters;

import com.csviri.configservice.api.model.paramter.ParameterType;

public class ParameterIdentifier {

	private String name;

	private String label;

	private int version;

	public ParameterIdentifier() {
	}

	public ParameterIdentifier(String name, String label, int version) {
		this.name = name;
		this.label = label;
		this.version = version;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

}
