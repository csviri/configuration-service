package com.csviri.configservice.parameters;

public class ConfigurationServiceException extends RuntimeException {

	public ConfigurationServiceException(String message) {
		super(message);
	}

	public ConfigurationServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public ConfigurationServiceException(Throwable cause) {
		super(cause);
	}

	public ConfigurationServiceException() {
	}
}
