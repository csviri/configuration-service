package com.csviri.configservice.parameters;

import javax.annotation.Resource;

import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

@Component
public class CacheableLabelService implements LabelService {

	private static final String DEFAULT_LABEL_KEY = "default-label";

	@Resource(name = "redisTemplate")
	private ValueOperations<String, String> valueOperations;

	@Override
	public String getDefaultLabel() {
		return valueOperations.get(DEFAULT_LABEL_KEY);
	}

	@Override
	public void setDefaultLabel(String label) {
		valueOperations.set(DEFAULT_LABEL_KEY, label);
	}
}
