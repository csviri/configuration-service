package com.csviri.configservice.parameters;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Nullable;
import javax.annotation.Resource;

import com.csviri.configservice.api.model.paramter.MapParameter;
import com.csviri.configservice.api.model.paramter.Parameter;
import com.csviri.configservice.api.model.paramter.StringListParameter;
import com.csviri.configservice.api.model.paramter.StringParameter;
import com.csviri.configservice.parameters.datamodel.ParameterMetadata;
import com.csviri.configservice.parameters.datamodel.ParameterVersionMetadata;

import org.springframework.stereotype.Service;

@Service
public class ParameterServiceImpl implements ParameterService {

	@Resource
	private ParameterRepository repository;

	@Resource
	private LabelService labelService;

	@Override public Parameter readParameter(String name, String label) {
		if (label == null) {
			label = labelService.getDefaultLabel();
		}
		Optional<ParameterMetadata> parameterMetadata = repository.getParameterMetadata(name, label);
		if (!parameterMetadata.isPresent()) throw new ParameterNotFoundException(name, label);
		ParameterVersionMetadata versionMetadata = findActualParameter(parameterMetadata.get());
		if (versionMetadata == null) {
			throw new ParameterActuallyNotAvailableException(name, label);
		}

		ParameterIdentifier identifier = new ParameterIdentifier(name, label, versionMetadata.getVersionId());
		switch (parameterMetadata.get().getType()) {
			case STRING: {
				String value = repository.getStringParamValue(identifier);
				return new StringParameter(name, label, versionMetadata.getValidFrom(),
						versionMetadata.getValidTo(), versionMetadata.getVersionId(), value);
			}
			case MAP: {
				Map<String, String> value = repository.getMapParamValue(identifier);
				return new MapParameter(name, label, versionMetadata.getValidFrom(),
						versionMetadata.getValidTo(), versionMetadata.getVersionId(), value);
			}
			case STRING_LIST: {
				List<String> value = repository.getListParamValue(identifier);
				return new StringListParameter(name, label, versionMetadata.getValidFrom(),
						versionMetadata.getValidTo(), versionMetadata.getVersionId(), value);
			}
		}
		throw new IllegalStateException("Something not implemented yet? :)");
	}

	// add consistency check?
	@Override public Parameter createParameter(Parameter param) {
		if (param.getLabel() == null) param.setLabel(labelService.getDefaultLabel());
		Optional<ParameterMetadata> parameterMetadata = repository.getParameterMetadata(param.getName(), param.getLabel());

		if (parameterMetadata.isPresent()) {
			repository.addNewParameterVersion(parameterMetadata.get(), param);
		} else {
			repository.addNewParameter(param);
		}
		return param;
	}

	// add consistency check?
	@Override public Parameter updateParameter(Parameter param) {
		if (param.getLabel() == null) param.setLabel(labelService.getDefaultLabel());
		repository.update(param);
		return param;
	}

	//add delete all if label not present? allow labels?
	@Override public void deleteParameter(String name, @Nullable String label) {
		if (label == null) label = labelService.getDefaultLabel();
		repository.delete(name, label);
	}

	private ParameterVersionMetadata findActualParameter(ParameterMetadata metadata) {
		List<ParameterVersionMetadata> versions = metadata.getVersionsIdentifiers();
		ParameterVersionMetadata found = null;
		Date now = new Date();
		for (ParameterVersionMetadata version : versions) {
			if (now.before(version.getValidTo()) && now.after(version.getValidFrom())) {
				if (found != null) throw new InvalidConfigurationException(metadata.getName(), metadata.getLabel());
				found = version;
			}
		}
		return found;
	}

}
