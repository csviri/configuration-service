package com.csviri.configservice.parameters;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.GONE, reason="Resource actually not available")
public class ParameterActuallyNotAvailableException extends ParameterException {

	public ParameterActuallyNotAvailableException(String paramName, String label) {
		super(paramName, label);
	}
}
