package com.csviri.configservice.parameters;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="Parameter not found")
public class ParameterNotFoundException extends ParameterException {

	public ParameterNotFoundException(String paramName, String label) {
		super(paramName, label);
	}

}
