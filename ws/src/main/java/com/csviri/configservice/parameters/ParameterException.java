package com.csviri.configservice.parameters;

public abstract class ParameterException extends ConfigurationServiceException {

	private String paramName;

	private String label;

	public ParameterException(String paramName, String label) {
		this.paramName = paramName;
		this.label = label;
	}

	@Override public String toString() {
		return "ParameterException{" +
				"paramName='" + paramName + '\'' +
				", label='" + label + '\'' +
				'}';
	}
}
