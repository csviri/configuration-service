package com.csviri.configservice.parameters.datamodel;

import java.util.ArrayList;
import java.util.List;

import com.csviri.configservice.api.model.paramter.ParameterType;

public class ParameterMetadata {

	private String name;

	private String label;

	private ParameterType type;

	private List<ParameterVersionMetadata> versionsIdentifiers;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public ParameterType getType() {
		return type;
	}

	public void setType(ParameterType type) {
		this.type = type;
	}

	public List<ParameterVersionMetadata> getVersionsIdentifiers() {
		if (versionsIdentifiers == null) {
			versionsIdentifiers = new ArrayList<>();
		}
		return versionsIdentifiers;
	}

	public void setVersionsIdentifiers(List<ParameterVersionMetadata> versionsIdentifiers) {
		this.versionsIdentifiers = versionsIdentifiers;
	}
}
