package com.csviri.configservice.parameters;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.csviri.configservice.api.model.paramter.MapParameter;
import com.csviri.configservice.api.model.paramter.Parameter;
import com.csviri.configservice.api.model.paramter.ParameterType;
import com.csviri.configservice.api.model.paramter.StringListParameter;
import com.csviri.configservice.api.model.paramter.StringParameter;
import com.csviri.configservice.parameters.datamodel.ParameterMetadata;
import com.csviri.configservice.parameters.datamodel.ParameterVersionMetadata;

import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

@Component
// todo cleanup
public class RedisParameterRepository implements ParameterRepository {

	// todo, time zones to add
	public static final String DATE_FORMAT = "yyyy-MM-dd hh:mm:ss";

	private static final String PARAMETER_TYPE_PREFIX = "param-type:";
	private static final String PARAMETER_PREFIX = "param:";

	private static final String VERSION_LIST_PREFIX = "versions:";

	private static final String VERSION_PREFIX = "version:";

	private static final String VERSION_VALID_FROM = "valid-from";
	private static final String VERSION_VALID_TO = "valid-to";

	@Resource(name = "redisTemplate")
	private HashOperations<String, String, String> hashOperations;

	@Resource(name = "redisTemplate")
	private ValueOperations<String, String> valueOperations;

	@Resource(name = "redisTemplate")
	private ListOperations<String, String> listOperations;

	@Resource(name = "redisTemplate")
	private RedisOperations<String, String> redisOperations;

	@Override public Optional<ParameterMetadata> getParameterMetadata(String name, String label) {
		ParameterMetadata res = new ParameterMetadata();
		ParameterType type = getParameterType(name);
		if (type == null) return Optional.empty();
		res.setName(name);
		res.setLabel(label);
		res.setType(type);

		List<Integer> versions = versions(name, label);
		// can happen that type exists for other labels
		if (versions.isEmpty()) return Optional.empty();

		for (Integer v : versions) {
			res.getVersionsIdentifiers().add(getParamVersion(name, label, v));
		}
		return Optional.of(res);
	}

	private ParameterType getParameterType(String name) {
		String type = valueOperations.get(PARAMETER_TYPE_PREFIX + name);
		if (type == null) return null;
		return ParameterType.valueOf(type);
	}

	@Override public void addNewParameterVersion(ParameterMetadata metadata, Parameter parameter) {
		ParameterVersionMetadata lastVersion = metadata.getVersionsIdentifiers().get(metadata.getVersionsIdentifiers().size() - 1);
		ParameterType type = metadata.getType();
		int newVersionId = (lastVersion.getVersionId() + 1);
		createNewVersionParam(parameter, type, newVersionId);
	}

	@Override public void addNewParameter(Parameter parameter) {
		ParameterType type = getParameterType(parameter);
		valueOperations.set(PARAMETER_TYPE_PREFIX + parameter.getName(), type.name());
		createNewVersionParam(parameter, type, 0);
	}

	private ParameterType getParameterType(Parameter parameter) {
		ParameterType type;
		if (parameter instanceof StringParameter) {
			type = ParameterType.STRING;
		} else if (parameter instanceof MapParameter) {
			type = ParameterType.MAP;
		} else if (parameter instanceof StringListParameter) {
			type = ParameterType.STRING_LIST;
		} else {
			throw new IllegalStateException();
		}
		return type;
	}

	@Override public void update(Parameter p) {
		String versionKey = VERSION_PREFIX + p.getName() + ":" + p.getLabel() + ":" + p.getVersion();

		redisOperations.delete(versionKey);
		Map<String, String> validity = new HashMap<>();
		validity.put(VERSION_VALID_FROM, dateFormat().format(p.getValidFrom()));
		validity.put(VERSION_VALID_TO, dateFormat().format(p.getValidTo()));
		hashOperations.putAll(versionKey, validity);

		setValue(getParameterType(p), p);
	}

	private void setValue(ParameterType type, Parameter parameter) {
		String key = PARAMETER_PREFIX + parameter.getName() + ":" + parameter.getLabel() + ":" + parameter.getVersion();

		switch (type) {
			case STRING: {
				StringParameter p = (StringParameter) parameter;
				valueOperations.set(key, p.getValue());
				break;
			}
			case MAP: {
				MapParameter p = (MapParameter) parameter;
				hashOperations.putAll(key, p.getValue());
				break;
			}
			case STRING_LIST: {
				StringListParameter p = (StringListParameter) parameter;
				listOperations.rightPushAll(key, p.getValue());
				break;
			}
		}
	}

	private void createNewVersionParam(Parameter parameter, ParameterType type, int version) {
		parameter.setVersion(version);

		listOperations.rightPush(VERSION_LIST_PREFIX + parameter.getName() + ":" + parameter.getLabel(), "" + version);

		Map<String, String> validity = new HashMap<>();
		validity.put(VERSION_VALID_FROM, dateFormat().format(parameter.getValidFrom()));
		validity.put(VERSION_VALID_TO, dateFormat().format(parameter.getValidTo()));
		hashOperations.putAll(VERSION_PREFIX + parameter.getName() + ":" + parameter.getLabel() + ":" + version, validity);

		setValue(type, parameter);
	}

	private List<Integer> versions(String name, String label) {
		return listOperations.range(VERSION_LIST_PREFIX + name + ":" + label, 0, -1)
				.stream()
				.map(v -> Integer.parseInt(v)).collect(Collectors.toList());
	}

	private ParameterVersionMetadata getParamVersion(String name, String label, int version) {
		try {
			ParameterVersionMetadata pvm = new ParameterVersionMetadata();
			Map<String, String> v = hashOperations.entries(VERSION_PREFIX + name + ":" + label + ":" + version);
			pvm.setVersionId(version);
			pvm.setValidFrom(dateFormat().parse(v.get(VERSION_VALID_FROM)));
			pvm.setValidTo(dateFormat().parse(v.get(VERSION_VALID_TO)));
			return pvm;
		} catch (ParseException e) {
			throw new IllegalStateException("Illegal Date format", e);
		}
	}

	@Override public String getStringParamValue(ParameterIdentifier parameterIdentifier) {
		String key = createParamKey(parameterIdentifier);
		return valueOperations.get(key);
	}


	@Override public List<String> getListParamValue(ParameterIdentifier parameterIdentifier) {
		return listOperations.range(createParamKey(parameterIdentifier), 0, -1);
	}

	@Override public Map<String, String> getMapParamValue(ParameterIdentifier parameterIdentifier) {
		return hashOperations.entries(createParamKey(parameterIdentifier));
	}

	@Override public void delete(String name, String label) {
		Optional<ParameterMetadata> metadata = getParameterMetadata(name, label);
		if (!metadata.isPresent()) throw new ParameterNotFoundException(name, label);

		for (ParameterVersionMetadata versionMetadata : metadata.get().getVersionsIdentifiers()) {
			redisOperations.delete(VERSION_PREFIX + name + ":" + label + ":" + versionMetadata.getVersionId());
			redisOperations.delete(PARAMETER_PREFIX + name + ":" + label + ":" + versionMetadata.getVersionId());
		}
		redisOperations.delete(VERSION_LIST_PREFIX + name + ":" + label);
		redisOperations.delete(PARAMETER_TYPE_PREFIX + name);
	}

	private SimpleDateFormat dateFormat() {
		return new SimpleDateFormat(DATE_FORMAT);
	}

	private String createParamKey(ParameterIdentifier pi) {
		return PARAMETER_PREFIX + pi.getName() + ":" + pi.getLabel() + ":" + pi.getVersion();
	}

}
