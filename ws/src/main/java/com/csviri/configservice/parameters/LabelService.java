package com.csviri.configservice.parameters;

public interface LabelService  {

	String getDefaultLabel();

	void setDefaultLabel(String label);

}
