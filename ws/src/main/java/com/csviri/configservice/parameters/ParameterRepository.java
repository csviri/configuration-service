package com.csviri.configservice.parameters;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.csviri.configservice.api.model.paramter.Parameter;
import com.csviri.configservice.parameters.datamodel.ParameterMetadata;

import org.springframework.stereotype.Component;

@Component
public interface ParameterRepository {

	Optional<ParameterMetadata> getParameterMetadata(String name, String label);

	void addNewParameterVersion(ParameterMetadata metadata, Parameter parameter);

	void addNewParameter(Parameter parameter);

	void update(Parameter parameter);

	String getStringParamValue(ParameterIdentifier parameterIdentifier);

	List<String> getListParamValue(ParameterIdentifier parameterIdentifier);

	Map<String,String> getMapParamValue(ParameterIdentifier parameterIdentifier);

	void delete(String name, String label);
}
