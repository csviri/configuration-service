package com.csviri.configservice.parameters;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.CONFLICT, reason="More configs for target/actual time")
public class InvalidConfigurationException extends ParameterException {


	public InvalidConfigurationException(String paramName, String label) {
		super(paramName, label);
	}
}
