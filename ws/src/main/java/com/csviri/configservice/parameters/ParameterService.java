package com.csviri.configservice.parameters;

import javax.annotation.Nullable;

import com.csviri.configservice.api.model.paramter.Parameter;

public interface ParameterService {

	Parameter readParameter(String name, @Nullable String label);

	Parameter createParameter(Parameter parameter);

	Parameter updateParameter(Parameter parameter);

	void deleteParameter(String name, @Nullable String label);
}
