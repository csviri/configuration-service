package com.csviri.configservice;

import java.net.URISyntaxException;

import com.csviri.configservice.api.model.paramter.Parameter;
import com.csviri.configservice.parameters.ParameterService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
public class ParameterController {

	private static final String PARAMETER_PATH = "/parameter";

	@Autowired
	private ParameterService parameterService;

	@RequestMapping(value = "/parameter/{name}", method = RequestMethod.GET)
	@ResponseBody
	public Parameter readParameter(@PathVariable String name, @RequestParam(name = "label", required = false) String label) {
		Parameter parameter = parameterService.readParameter(name, label);
		return parameter;
	}

	// label in content
	@RequestMapping(value = "/parameter", method = RequestMethod.POST)
	@ResponseBody
	@ResponseStatus(code = HttpStatus.CREATED)
	public Parameter createParameter(@RequestBody Parameter parameter) {
		return parameterService.createParameter(parameter);
	}

	// label in content
	@RequestMapping(value = "/parameter", method = RequestMethod.PUT)
	@ResponseStatus(code = HttpStatus.OK)
	public Parameter updateParameter(@RequestBody Parameter parameter) {
		return parameterService.updateParameter(parameter);
	}

	@RequestMapping(value = "/parameter/{name}", method = RequestMethod.DELETE)
	@ResponseStatus(code = HttpStatus.OK)
	public void deleteParameter(@PathVariable String name, @RequestParam(name = "label", required = false) String label) {
		parameterService.deleteParameter(name, label);
	}

}
