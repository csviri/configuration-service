package com.csviri.configservice.api.client;

import java.util.Date;

import com.csviri.configservice.api.model.paramter.MapParameter;
import com.csviri.configservice.api.model.paramter.StringParameter;

import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;


public class SpringConfigServiceClientIntegrationTest {

	SpringConfigServiceClient client = new SpringConfigServiceClient("http://localhost:8080/");

	@Test
	public void creatingParameter() {
		String paramName = "refresh-rate5";
		deleteParam(paramName);

		StringParameter p = new StringParameter();
		p.setName(paramName);
		p.setValidFrom(new Date(System.currentTimeMillis() - 1000));
		p.setValidTo(new Date(System.currentTimeMillis() + 300000));
		p.setValue("2.2");

		StringParameter res = client.createNewParameterOrVersion(p);

		assertEquals("2.2", res.getValue());
		assertEquals(0, res.getVersion());
		assertEquals("sf",res.getLabel());
	}


	@Test
	public void stringParamGet() {
		StringParameter param = client.readParameter("hdn-version");
		assertEquals("15.10", param.getValue());
		assertEquals(0, param.getVersion());
	}

	@Test
	public void stringParamGetInvalidLabel() {
		try {
			client.readParameter("hdn-version", "xxx");
			fail();
		} catch (HttpClientErrorException e) {
			assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode());
		}
	}

	@Test
	public void mapParamGet() {
		MapParameter param = client.readParameter("source");
		assertEquals("git", param.getValue().get("type"));
		assertEquals(1, param.getVersion());
	}

	private void deleteParam(String name) {
		try {
			client.deleteParameter(name);
		} catch (HttpClientErrorException e) {
			if (e.getStatusCode() != HttpStatus.NOT_FOUND) throw new IllegalStateException(e.getStatusCode().toString());
		}
	}

}