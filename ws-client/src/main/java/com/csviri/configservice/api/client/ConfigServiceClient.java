package com.csviri.configservice.api.client;

import com.csviri.configservice.api.model.paramter.Parameter;

public interface ConfigServiceClient {

	void setDefaultLabel(String label);

	<T extends Parameter> T readParameter(String name);

	<T extends Parameter> T readParameter(String name, String Label);

	<T extends Parameter> T createNewParameterOrVersion(T p);

	<T extends Parameter> void updateParameterVersion(T p);

	void deleteParameter(String name);

	void deleteParameter(String name, String label);
}
