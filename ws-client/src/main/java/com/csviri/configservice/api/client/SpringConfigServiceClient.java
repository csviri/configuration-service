package com.csviri.configservice.api.client;

import java.util.HashMap;

import com.csviri.configservice.api.model.paramter.Parameter;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

public class SpringConfigServiceClient implements ConfigServiceClient {

	private String urlBase = "http://localhost:8080/parameter/";
	private static final String PARAMETER_PATH = "parameter/";

	private RestTemplate restTemplate = new RestTemplate();

	public SpringConfigServiceClient(String url) {
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		this.urlBase = url;
	}

	@Override public void setDefaultLabel(String label) {

	}

	public <T extends Parameter> T readParameter(String name) {
		HashMap<String, String> vars = new HashMap<>();
		vars.put("name", name);
		return (T) restTemplate.getForObject(paramPath() + "{name}", Parameter.class, vars);
	}

	public <T extends Parameter> T readParameter(String name, String label) {
		HashMap<String, String> vars = new HashMap<>();
		vars.put("label", label);
		vars.put("name", name);
		return (T) restTemplate.getForObject(paramPath() + "{name}?label={label}", Parameter.class, vars);
	}

	@Override public <T extends Parameter> T createNewParameterOrVersion(T p) {
		return (T) restTemplate.postForObject(paramPath(), p, Parameter.class, new HashMap<String, Object>(0));
	}

	@Override public void updateParameterVersion(Parameter p) {
		restTemplate.put(urlBase + PARAMETER_PATH, p, new HashMap<String, Object>());
	}

	@Override public void deleteParameter(String name) {
		HashMap<String, String> vars = new HashMap<>();
		vars.put("name", name);
		restTemplate.delete(paramPath() + "{name}", vars);
	}

	@Override public void deleteParameter(String name, String label) {
		HashMap<String, String> vars = new HashMap<>();
		vars.put("label", label);
		vars.put("name", name);
		restTemplate.delete(paramPath() + "{name}?label={label}", Parameter.class, vars);
	}

	private String paramPath() {
		return urlBase + PARAMETER_PATH;
	}
}
